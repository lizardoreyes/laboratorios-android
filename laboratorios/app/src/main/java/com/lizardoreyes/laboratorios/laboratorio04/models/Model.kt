package com.lizardoreyes.laboratorios.laboratorio04.models

data class Model(
    val name: String,
    val description: String = "no description"
) {
    override fun toString(): String = name
}