package com.lizardoreyes.laboratorios.laboratorio01

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.lizardoreyes.laboratorios.R
import kotlinx.android.synthetic.main.activity_main_laboratorio_1.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_laboratorio_1)

        btnConfirmar.setOnClickListener {
            val edtEdad = edtEdad.text.toString().toInt()

            // 122 es la edad maxima que ha vivido una persona en la historia
            val mensaje = when (edtEdad) {
                in 0 until 18 -> "Usted es menor de edad"
                in 18..122 -> "Usted es mayor de edad"
                else ->  "Ingrese una edad valida"
            }

            tvResultado.text = mensaje
        }
    }
}