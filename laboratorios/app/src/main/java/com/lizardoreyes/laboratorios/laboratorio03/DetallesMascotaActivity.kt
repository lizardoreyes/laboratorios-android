package com.lizardoreyes.laboratorios.laboratorio03

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.lizardoreyes.laboratorios.R
import kotlinx.android.synthetic.main.activity_detalles_mascota.*

class DetallesMascotaActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detalles_mascota)

        val extras = intent.extras
        val nombre = extras?.getString("nombre") ?: "Sin nombre"
        val edad = extras?.getString("edad") ?: "Sin edad"
        val mascota = extras?.getString("mascota") ?: "Sin mascota"
        val vacunas = extras?.getString("vacunas") ?: "Sin vacunas"

        val mascotaImagen = when(mascota) {
            "Perro" -> R.drawable.perro
            "Gato" -> R.drawable.gato
            "Conejo" -> R.drawable.conejo
            else -> R.drawable.desconocido
        }

        tvhNombre.text = nombre
        tvhEdad.text = edad
        imvMascota.setImageResource(mascotaImagen)
        tvhVacunas.text = vacunas
    }
}