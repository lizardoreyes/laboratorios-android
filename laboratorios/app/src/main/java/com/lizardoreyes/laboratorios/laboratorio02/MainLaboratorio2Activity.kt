package com.lizardoreyes.laboratorios.laboratorio02

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.lizardoreyes.laboratorios.R
import kotlinx.android.synthetic.main.activity_main_laboratorio_2.*

class MainLaboratorio2Activity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_laboratorio_2)

        btnVerificar.setOnClickListener {

            val anio = edtAnio.text.toString().toInt()

            val information = when (anio) {
                in 1930..1948 -> Pair("6.300.000", R.drawable.austeridad)
                in 1949..1968 -> Pair("12.200.000", R.drawable.ambicion)
                in 1969..1980 -> Pair("9.300.000", R.drawable.obsesion_exito)
                in 1981..1993 -> Pair("7.200.000", R.drawable.frustracion)
                in 1994..2010 -> Pair("7.800.000", R.drawable.irreverencia)
                else -> Pair("No perteneces a ninguna generación", R.drawable.desconocido)
            }

            val (text, image) = information

            tvResultadoGeneracion.text = "Aproximadamente $text personas"
            imageView.setImageResource(image)

        }
    }
}