package com.lizardoreyes.laboratorios.laboratorio04

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ArrayAdapter
import com.lizardoreyes.laboratorios.R
import com.lizardoreyes.laboratorios.laboratorio04.models.Model
import kotlinx.android.synthetic.main.activity_main_laboratorio_4.*

class MainLaboratorio4Activity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_laboratorio_4)

        val marcas = arrayListOf(
            Model("Toyota"), Model("Volkswagen"), Model("Ford"), Model("Honda"),
            Model("Nissan"), Model("Hyundai"), Model("Chevrolet"), Model("Kia"),
            Model("Mercedes Benz"), Model("BMW"), Model("Renault"), Model("Audi"),
            Model("Maruti"), Model("Peugeot"), Model("Mazda"), Model("Jeep"),
            Model("Geely"), Model("Fiat", "Skoda"), Model("Changan"))

        spMarcas.adapter = ArrayAdapter(this, R.layout.style_item_spinner, marcas)

        // NO SE TERMINO DE EXPLICAR COMO CREAR UNA VENTANA MODAL
    }
}