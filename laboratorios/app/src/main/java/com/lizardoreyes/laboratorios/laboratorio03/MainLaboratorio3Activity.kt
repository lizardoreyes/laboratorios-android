package com.lizardoreyes.laboratorios.laboratorio03

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.lizardoreyes.laboratorios.R
import kotlinx.android.synthetic.main.activity_main_laboratorio_3.*

class MainLaboratorio3Activity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_laboratorio_3)

        btnEnviar.setOnClickListener {
            val nombre = edtNombre.text.toString()
            val edad = edtEdad.text.toString()

            if (nombre.isEmpty()) {
                Toast.makeText(this, "Ingrese el nombre de su mascota", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            if (edad.isEmpty()) {
                Toast.makeText(this, "Ingrese la edad de su mascota", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            val mascota: String = when {
                rbPerro.isChecked -> {
                    rbPerro.text.toString()
                }
                rbConejo.isChecked -> {
                    rbConejo.text.toString()
                }
                else -> {
                    rbGato.text.toString()
                }
            }

            val vacunas: ArrayList<String> = arrayListOf()
            if (chkRabia.isChecked) vacunas.add(chkRabia.text.toString())
            if (chkClamidofila.isChecked) vacunas.add(chkClamidofila.text.toString())
            if (chkLeishmania.isChecked) vacunas.add(chkLeishmania.text.toString())
            if (chkLeucemia.isChecked) vacunas.add(chkLeucemia.text.toString())
            if (chkParvovirus.isChecked) vacunas.add(chkParvovirus.text.toString())
            if (chkTraqueitis.isChecked) vacunas.add(chkTraqueitis.text.toString())

            val vacunasText = if (vacunas.isNotEmpty()) vacunas.reduce { acc, s ->  "$acc\n$s" } else "Sin vacunas"

            val bundle = Bundle().apply {
                putString("nombre", nombre)
                putString("edad", edad)
                putString("mascota", mascota)
                putString("vacunas", vacunasText)
            }

            val intent = Intent(this, DetallesMascotaActivity::class.java)
            intent.putExtras(bundle)
            startActivity(intent)
        }
    }
}